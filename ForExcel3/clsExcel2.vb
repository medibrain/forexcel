﻿'Imports Microsoft.Office.Interop.Excel
'Excelバージョン差異吸収のため、遅延バインディング化

Imports System.Runtime.InteropServices.Marshal
Imports System.Runtime.InteropServices
Imports System.Data
Imports System.Windows.Forms


Public Class clsExcel2 : Implements IDisposable

    Dim objExcel As Object
    Dim objWorkBooks As Object
    Dim objWorkBook As Object
    Dim objWorkSheets As Object
    Dim objWorkSheet As Object
    Dim strSavePath As String = String.Empty

    ''' <summary>
    ''' エクセルのセルの水平配置位置の値
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum HAlign As Integer
        Left = -4131
        Right = -4152
        Center = -4108
    End Enum

    ''' <summary>
    ''' 出力ヘッダの位置
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum HeaderPosition As Integer
        Left
        Right
        Center
    End Enum

#Region "Excelオブジェクト作成（保存前提）"

    ''' <summary>
    ''' Excelオブジェクト作成（保存前提）
    ''' </summary>
    ''' <returns></returns>

    Private Function CreateExcelAsSave() As Boolean

        Try
            GetSavePath()
            If strSavePath = String.Empty Then Return False

            objExcel = CreateObject("Excel.Application")

            Return True
        Catch ex As Exception
            MsgBox("Excelオブジェクトの作成に失敗しました。" & vbCrLf & _
                   ex.Message, MsgBoxStyle.Exclamation)
            Return False
        Finally

        End Try

    End Function
#End Region

#Region "Excelオブジェクト作成（作成のみ）"

    ''' <summary>
    ''' Excelオブジェクト作成（作成のみ）
    ''' </summary>

    Public Function CreateExcelNoSave() As Object

        Try
            objExcel = CreateObject("Excel.Application")
            Return objExcel

        Catch ex As Exception
            MsgBox("Excelオブジェクトの作成に失敗しました。" & vbCrLf & _
                   ex.Message, MsgBoxStyle.Exclamation)
            Return Nothing
        Finally

        End Try

    End Function
#End Region

#Region "保存先設定"

    ''' <summary>
    ''' 保存先設定
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetSavePath(Optional ByVal strFileName As String = "Book1.xls")
        Dim fd As New SaveFileDialog

        Try
            fd.Title = "保存先選択"
            fd.Filter = "*.xls(Excelファイル)|*.xls"
            fd.FilterIndex = 0
            fd.DefaultExt = "*.xls"
            fd.FileName = strFileName
            If fd.ShowDialog = DialogResult.OK Then
                strSavePath = fd.FileName
            End If

        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "既存のワークブックを開く（保存前提）"

    ''' <summary>
    ''' 既存のワークブック開く（保存前提）
    ''' </summary>
    ''' <param name="strOpenPath">既存のファイルパス</param>
    ''' <param name="flgVisible">可視化フラグ</param>
    ''' <param name="strSheetName">シート名</param>
    ''' <returns>True:成功,False:失敗、キャンセル</returns>
    ''' <remarks></remarks>
    Public Overloads Function OpenWorkBook(ByVal strOpenPath As String, _
                                           ByVal flgVisible As Boolean, _
                                           Optional ByVal strSheetName As String = "Sheet1") As Boolean
        Try
            If Not CreateExcelAsSave() Then Return False

            objExcel.Visible = flgVisible

            objWorkBooks = objExcel.Workbooks
            objWorkBook = objWorkBooks.Open(strOpenPath)
            objWorkSheet = objWorkBook.Worksheets(strSheetName)

            Return True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Return False
        End Try

    End Function
#End Region

#Region "既存のワークブックを開く（作成のみ）"

    ''' <summary>
    ''' 既存のワークブック開く（作成のみ）
    ''' </summary>
    ''' <param name="strOpenPath">既存のファイルパス</param>
    ''' <param name="strSheetName">シート名</param>
    ''' <returns>True:成功,False:失敗、キャンセル</returns>
    ''' <remarks></remarks>
    Public Overloads Function OpenWorkBook(ByVal strOpenPath As String, _
                                           ByVal strSheetName As String) As Boolean

        Try
            If IsNothing(objExcel) Then
                MsgBox("CreateExcelNoSaveを実行してください")
                Return False
            End If


            objWorkBooks = objExcel.Workbooks
            objWorkBook = objWorkBooks.Open(strOpenPath)
            For Each b In objWorkBook.worksheets
                If b.name = strSheetName Then
                    objWorkSheet = objWorkBook.Worksheets(strSheetName)
                    Return True
                End If
            Next

            Return False


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Return False
        End Try

    End Function
#End Region

#Region "既存のワークブックを開く（作成のみ,シート未指定）"

    ''' <summary>
    ''' 既存のワークブック開く（作成のみ,シート未指定）
    ''' </summary>
    ''' <param name="strOpenPath">既存のファイルパス</param>
    ''' <returns>True:成功,False:失敗、キャンセル</returns>
    ''' <remarks></remarks>
    Public Overloads Function OpenWorkBook(ByVal strOpenPath As String) As Boolean

        Try
            If IsNothing(objExcel) Then
                MsgBox("CreateExcelNoSaveを実行してください")
                Return False
            End If


            objWorkBooks = objExcel.Workbooks
            objWorkBook = objWorkBooks.Open(strOpenPath)

            Return True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Return False
        End Try

    End Function
#End Region


#Region "出力文字列"
    ''' <summary>
    ''' 出力文字列
    ''' </summary>
    ''' <param name="strOut">出力文字列</param>
    ''' <param name="strSheetName">シート名</param>
    ''' <param name="StartColumn">出力する列番号</param>
    ''' <param name="StartRow">出力する行番号</param>
    ''' <param name="strFontName">フォント名</param>
    ''' <param name="intFontSize">フォントサイズ</param>
    ''' <param name="intHAlign">水平位置</param>
    ''' <param name="flgAutoFit">オートフィットフラグ</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function OutputExtensionString(ByVal strOut As String, _
                                ByVal strSheetName As String, _
                                ByVal StartColumn As Integer, _
                                ByVal StartRow As Integer, _
                                Optional ByVal strFontName As String = "ＭＳ ゴシック", _
                                Optional ByVal intFontSize As Integer = 20, _
                                Optional ByVal intHAlign As HAlign = HAlign.Left, _
                                Optional ByVal flgAutoFit As Boolean = True) As Boolean

        Try
            objWorkSheet = objWorkBook.worksheets(strSheetName) 'シートの選択
            With objWorkSheet.cells(StartRow, StartColumn)

                .NumberFormatLocal = "G/標準"
                .FormulaR1C1 = String.Empty
                .font.name = strFontName
                .font.size = intFontSize
                .HorizontalAlignment = intHAlign
                .wraptext = False '折り返して表示を解除
                .ShrinkToFit = False '縮小して全体を表示を解除
                .value = strOut

                'オートフィット
                If flgAutoFit Then objWorkSheet.rows(StartRow).entirerow.autofit()


            End With
            Return True

        Catch ex As Exception
            MsgBox("文字列出力" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation)
            CloseExcelNoSave()
            Return False
        Finally

        End Try
    End Function
#End Region

#Region "セルの値を取得"
    Public Function GetCellValue(ByVal strSheet As String, _
                                 ByVal strRange As String) As String

        Dim strRet As String = String.Empty
        objWorkSheet = objWorkBook.worksheets(strSheet)
        objWorkSheet.unprotect("medi1202")
        strRet = objWorkSheet.range(strRange).value

        objWorkSheet = Nothing
        objWorkBook.close(False)
        ReleaseComObject(objWorkBook)

        Return strRet

    End Function
#End Region


#Region "協会けんぽ用作業表作成"

    Public Function CreateSagyoSheet(ByVal strWSName As String, _
                                    ByVal dt As DataTable) As Boolean

        Dim s As Object
        Try
            '作業表を削除する
            For Each s In objWorkBook.worksheets
                If s.name = strWSName Then
                    objExcel.DisplayAlerts = False
                    s.delete()
                    objExcel.DisplayAlerts = True
                    Exit For
                End If
            Next

            objWorkSheet = objWorkBook.sheets.add()

            With objWorkSheet

                .name = strWSName

                'ヘッダ
                For c As Integer = 0 To dt.Columns.Count - 1
                    .cells(1, c + 1) = dt.Columns(c).ColumnName
                    Select Case .cells(1, c + 1).value
                        Case "セット番号", "記号番号", "項番" : .columns(c + 1).numberformatlocal = "@"
                    End Select
                Next

                'データ
                For r As Integer = 0 To dt.Rows.Count - 1

                    'データ入れる
                    For c As Integer = 0 To dt.Columns.Count - 1
                        .cells(r + 2, c + 1) = dt.Rows(r)(c).ToString
                    Next

                    '連番つける
                    .cells(r + 2, 1) = r + 1
                Next
                .columns.autofit()


                If dt.Rows.Count > 0 Then
                    '色番号で色つけ
                    For r = 2 To .cells(1, 1).end(-4121).row
                        If Not IsNothing(.cells(r, 26).value) AndAlso _
                        .cells(r, .cells.find("色").column).value.ToString <> String.Empty Then

                            .range(.cells(r, 1), .cells(r, 11)).interior.colorindex = .cells(r, .cells.find("色").column).value
                        End If
                    Next
                End If


                '色番号列削除
                .columns(.cells.find("色").column).delete()

                'シートの体裁（山内希望）

                'フォント関連
                .Cells.Font.Name = "ＭＳ ゴシック"
                .Cells.Font.Size = 10

                .Columns("a:a").Font.Size = 10
                .Columns("b:b").Font.Size = 8
                .Columns("c:c").Font.Size = 12
                .Columns("e:f").Font.Size = 14


                .Columns.AutoFit()

                '各行の高さ
                .UsedRange.RowHeight = 20

                .Columns(.Cells.Find("国名").Column).ColumnWidth = 10


                '外国語の広さ
                '.Columns("s:s").ColumnWidth = 10
                .Columns(.Cells.Find("外国語").Column).ColumnWidth = 10


                'メモの広さ
                '.Columns("u:u").ColumnWidth = 20
                .Columns(.Cells.Find("memo1").Column).ColumnWidth = 20


                'ヘッダ行
                .Rows("1:1").RowHeight = 35
                .Rows("1:1").WrapText = True

                '列の広さ
                '.Columns("c:d").ColumnWidth = 7
                .Columns(.Cells.Find("セット番号").Column).ColumnWidth = 5
                .Columns(.Cells.Find("支部コード").Column).ColumnWidth = 4


                .Columns("e:f").ColumnWidth = 7
                .Columns("g:h").ColumnWidth = 5
                .Columns("L:p").ColumnWidth = 6

                '枠線
                .UsedRange.Borders.LineStyle = 1
                .UsedRange.Borders.Weight = 1

                '印刷設定
                .PageSetup.Zoom = 70
                .PageSetup.Orientation = 2
                .PageSetup.TopMargin = 20
                .PageSetup.BottomMargin = 30
                .PageSetup.RightMargin = 10
                .PageSetup.LeftMargin = 10

                .PageSetup.PrintTitleRows = "$1:$1"
                .PageSetup.CenterFooter = "&P/&N"

                .PageSetup.PrintArea = .Range("a:y").Address

            End With


            objExcel.DisplayAlerts = False
            objWorkBook.save()
            objExcel.DisplayAlerts = True
            Return True

        Catch ex As Exception

            MessageBox.Show(ex.Message)
            Return False
        Finally
            CloseExcelNoSave()

        End Try

    End Function
#End Region

#Region "協会けんぽ用セット番号フォルダ委託区分作成"

    Public Function CreateSetItakuSheet(ByVal strWSName As String, _
                                    ByVal ds As DataSet) As Boolean

        Dim s As Object
        Try
            '削除する
            For Each s In objWorkBook.worksheets
                If s.name = strWSName Then
                    objExcel.DisplayAlerts = False
                    s.delete()
                    objExcel.DisplayAlerts = True
                    Exit For
                End If
            Next

            objWorkSheet = objWorkBook.sheets.add()

            With objWorkSheet

                .name = strWSName

                For cnt As Integer = 0 To ds.Tables.Count - 1

                    'データ
                    For r As Integer = 0 To ds.Tables(cnt).Rows.Count - 1
                        For c As Integer = 0 To ds.Tables(cnt).Columns.Count - 1
                            'If r = 0 Then
                            '    .cells(r + 1, c + 1 + cnt) = ds.Tables(cnt).Columns(c).ColumnName
                            'Else
                            .cells(r + 2, c + 1 + cnt) = ds.Tables(cnt).Rows(r)(c).ToString
                            'End If
                        Next
                    Next

                Next
                '.rows(1).insert(-4121)
                .cells(1, 1).value = "委託区分=1"
                .cells(1, 2).value = "委託区分=2"
                .cells(1, 3).value = "委託区分=1&2"
                .columns.autofit()



            End With


            objExcel.DisplayAlerts = False
            objWorkBook.save()
            objExcel.DisplayAlerts = True
            Return True

        Catch ex As Exception

            MessageBox.Show(ex.Message)
            Return False
        Finally
            CloseExcelNoSave()

        End Try

    End Function
#End Region


#Region "セルに値をセット"
    ''' <summary>
    ''' エクセル、ワード、pdfの枚数を管理表に入れる
    ''' </summary>
    ''' <param name="strSheet">シート名</param>
    ''' <param name="dt">DataTable</param>
    ''' <returns>エラーのあった項番リスト</returns>
    ''' <remarks></remarks>
    Public Function SetCellValue(ByVal strSheet As String, _
                                 ByVal dt As DataTable) As List(Of String)

        Dim rng As Object = Nothing
        Dim rngExcelCnt As Object = Nothing
        Dim lstErr As New List(Of String)
        Dim strErr As String = String.Empty

        Try
            objWorkSheet = objWorkBook.worksheets(strSheet)
            rngExcelCnt = objWorkSheet.columns(objWorkSheet.cells.find("Chkエクセル数").column)
            For Each value As DataRow In dt.Rows
                strErr = value("項番").ToString()
                rng = objWorkSheet.columns(objWorkSheet.cells.find("項番").column)

                If value("項番").ToString.Length < 11 Then Continue For

                rng = rng.find(value("項番").ToString().Substring(0, 11))
                'rng = rng.find(value("項番").ToString())

                If IsNothing(rng) Then
                    lstErr.Add(value("項番").ToString() & Space(1) & "項番が見つかりません")
                    Continue For
                End If


                If dt.Columns.Contains("Chkエクセル数") Then objWorkSheet.cells(rng.row, rngExcelCnt.column).value = value("Chkエクセル数").ToString
                If dt.Columns.Contains("Chkワード数") Then objWorkSheet.cells(rng.row, rngExcelCnt.column + 1).value = value("Chkワード数").ToString
                If dt.Columns.Contains("Chkpdf数") Then objWorkSheet.cells(rng.row, rngExcelCnt.column + 2).value = value("Chkpdf数").ToString

                If dt.Columns.Contains("Chk合計点数") Then
                    objWorkSheet.cells(rng.row, rngExcelCnt.column + 3).value = value("Chk合計点数").ToString
                    If Int32.Parse(value("Chk合計点数")) = 0 Then
                        objWorkSheet.cells(rng.row, rngExcelCnt.column + 3).interior.colorindex = 3
                    End If
                End If


                If dt.Columns.Contains("翻訳直後フォルダ") Then
                    objWorkSheet.cells(rng.row, rng.column + 24).value = _
                        DateTime.Now.ToString("yyyy/MM/dd") & Space(1) & _
                        value("翻訳直後フォルダ").ToString & Space(1) & _
                        value("pdf").ToString & Space(1) & _
                        value("docx").ToString

                    objWorkSheet.cells(rng.row, rng.column + 25).value = _
                        value("set_folder").ToString & Space(1) & _
                        value("set_pdf").ToString & Space(1) & _
                        value("set_docx").ToString & Space(1) & _
                        value("set_xlsx").ToString & Space(1)

                End If

                rng = Nothing
            Next

            Return lstErr

        Catch ex As Exception

            MsgBox(strErr & vbCrLf & ex.Message)
            Return lstErr
        Finally
            objWorkBook.close(True)
            ReleaseComObject(objWorkBook)
            ReleaseComObject(objExcel)

        End Try

    End Function
#End Region


#Region "UsedRangeの取得"
    ''' <summary>
    ''' 使用領域の取得
    ''' </summary>
    ''' <param name="strSheet">シート名</param>
    ''' <param name="intHeaderRow">ヘッダの行</param>
    ''' <param name="intDataStartRow">データの開始行</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Public Function GetUsedRange(ByVal strSheet As String, _
                                 ByVal intHeaderRow As Integer, _
                                 ByVal intDataStartRow As Integer) As DataTable


        Dim retDT As New DataTable
        objWorkSheet = objWorkBook.worksheets(strSheet)

        Dim usedrng As Object
        usedrng = objWorkSheet.usedrange

        For c As Integer = 0 To usedrng.columns.count
            retDT.Columns.Add(objWorkSheet.cells(intHeaderRow, c + 1).value, System.Type.GetType("System.String"))
        Next

        Dim dr As DataRow

        For r As Integer = intDataStartRow To usedrng.rows.count
            dr = retDT.NewRow
            For c = 0 To retDT.Columns.Count - 1
                dr(c) = objWorkSheet.cells(r, c + 1).value
            Next
            retDT.Rows.Add(dr)
        Next
        retDT.AcceptChanges()

        ReleaseComObject(usedrng)
        ReleaseComObject(objWorkSheet)

        Return retDT


    End Function
#End Region


#Region "シート名取得"

    ''' <summary>
    ''' シート名取得
    ''' </summary>
    ''' <param name="strOpenPath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetSheetNames(ByVal strOpenPath As String) As ArrayList
        Dim arrSheetName As New ArrayList
        'Dim ws As Object = Nothing
        'Dim wss As Object = Nothing

        Try
            If IsNothing(objExcel) Then objExcel = CreateObject("Excel.Application")
            If IsNothing(objWorkBooks) Then objWorkBooks = objExcel.Workbooks
            If IsNothing(objWorkBook) Then objWorkBook = objWorkBooks.Open(strOpenPath)


            For Each Me.objWorkSheet In objWorkBook.worksheets
                arrSheetName.Add(objWorkSheet.name)
            Next


            'wss = objWorkBook.worksheets

            'For Each ws In wss
            '    arrSheetName.Add(ws.name)
            'Next
            'ReleaseComObject(ws)
            'ReleaseComObject(wss)
            'ws = Nothing
            'wss = Nothing

            Return arrSheetName

        Catch ex As Exception
            Return Nothing

        Finally
            CloseExcelNoSave()
        End Try

    End Function
#End Region


#Region "フィールド名出力"


    ''' <summary>
    ''' フィールド名出力
    ''' </summary>
    ''' <param name="arrOut">出力する項目の配列</param>
    ''' <param name="strSheetName">出力するシート名</param>
    ''' <param name="StartRow">最初の行</param>
    ''' <param name="StartColumn">最初の列</param>
    ''' <param name="strFontName">フォント名</param>
    ''' <param name="intFontSize">フォントサイズ</param>
    ''' <param name="intHAlign">文字位置</param>
    ''' <param name="flgAutoFit">オートフィット</param>
    ''' <param name="flgInsert">挿入フラグ</param>
    ''' <returns>True：成功、False：失敗</returns>
    ''' <remarks></remarks>
    Public Function OutputFieldStrings(ByVal arrOut As ArrayList, _
                               Optional ByVal strSheetName As String = "Sheet1", _
                               Optional ByVal StartRow As Integer = 1, _
                               Optional ByVal StartColumn As Integer = 1, _
                               Optional ByVal strFontName As String = "ＭＳ ゴシック", _
                               Optional ByVal intFontSize As Integer = 11, _
                               Optional ByVal intHAlign As HAlign = HAlign.Left, _
                               Optional ByVal flgAutoFit As Boolean = True, _
                               Optional ByVal flgInsert As Boolean = True) As Boolean

        Try
            objWorkSheet = objWorkBook.worksheets(strSheetName) 'シートの選択

            If flgInsert Then objWorkSheet.rows(StartRow).insert()

            With objWorkSheet.rows(StartRow)
                .NumberFormatLocal = "G/標準"
                .FormulaR1C1 = String.Empty
                .font.name = strFontName
                .font.size = intFontSize
                .HorizontalAlignment = intHAlign
                .wraptext = False '折り返して表示を解除
                .ShrinkToFit = False '縮小して全体を表示を解除

                For c As Integer = StartColumn To arrOut.Count
                    objWorkSheet.cells(StartRow, c).value = arrOut(c - 1)
                Next

                'オートフィット
                If flgAutoFit Then objWorkSheet.rows(StartRow).entirerow.autofit()


            End With
            Return True

        Catch ex As Exception
            MsgBox("文字列出力" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation)
            CloseExcelNoSave()
            Return False
        Finally

        End Try
    End Function
#End Region

#Region "シートヘッダ出力"
    ''' <summary>
    ''' シートヘッダ出力
    ''' </summary>
    ''' <param name="strOut">出力文字列</param>
    ''' <param name="strSheetName">出力シート名</param>
    ''' <param name="HeaderPosition">位置</param>
    ''' <param name="strFontName">フォント名</param>
    ''' <param name="intFontSize">フォントサイズ</param>
    ''' <param name="intHAlign"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function OutHeader(ByVal strOut As String, _
                                ByVal strSheetName As String, _
                                ByVal HeaderPosition As HeaderPosition, _
                                Optional ByVal strFontName As String = "ＭＳ ゴシック", _
                                Optional ByVal intFontSize As Integer = 20, _
                                Optional ByVal intHAlign As HAlign = HAlign.Left) As Boolean

        Try
            objWorkSheet = objWorkBook.worksheets(strSheetName) 'シートの選択
            With objWorkSheet.pagesetup
                Select Case HeaderPosition
                    Case clsExcel2.HeaderPosition.Left
                        .LeftHeader = strOut

                    Case clsExcel2.HeaderPosition.Right
                        .RightHeader = strOut

                    Case clsExcel2.HeaderPosition.Center
                        .CenterHeader = strOut

                End Select

            End With
            Return True
        Catch ex As Exception

            Return False

        End Try
    End Function
#End Region

#Region "一覧出力"

    ''' <summary>
    ''' エクセルへ一覧出力
    ''' </summary>
    ''' <param name="dt">datatable</param>
    ''' <param name="lstInt">数値型の列番号(0始まり)</param>
    ''' <param name="strSheetName">シート名</param>
    ''' <param name="StartColumn">開始列番号</param>
    ''' <param name="StartRow">開始行番号</param>
    ''' <returns>成功=true、失敗=false</returns>
    ''' <remarks></remarks>
    Public Overloads Function ExportData(ByVal dt As Data.DataTable, _
                               Optional ByVal lstInt As List(Of Integer) = Nothing, _
                               Optional ByVal strSheetName As String = "Sheet1", _
                               Optional ByVal StartColumn As Integer = 1, _
                               Optional ByVal StartRow As Integer = 1) As Boolean

        Try

            'DataTableから2次元配列にコピー
            'Excelに貼り付ける時、この方が高速
            Dim r As Integer, c As Integer
            Dim tmpArr(dt.Rows.Count, dt.Columns.Count) As Object

            For r = 0 To dt.Rows.Count - 1
                For c = 0 To dt.Columns.Count - 1
                    '数値型の列がある場合はダブルコーテーションつけない
                    If Not IsNothing(lstInt) AndAlso lstInt.Contains(c) Then
                        If Not IsDBNull(dt.Rows(r)(c)) Then tmpArr(r, c) = dt.Rows(r)(c)
                    Else
                        If Not IsDBNull(dt.Rows(r)(c)) Then tmpArr(r, c) = "" & dt.Rows(r)(c) & ""
                    End If
                    'If Not IsDBNull(dt.Rows(r)(c)) Then tmpArr(r, c) = "" & dt.Rows(r)(c) & ""
                Next
            Next

            objWorkSheet = objWorkBook.worksheets(strSheetName) 'シートの選択

            objWorkSheet.Cells.NumberFormat = "@" '値を文字列に
            objWorkSheet.pagesetup.printarea = "" '印刷範囲リセット

            Dim rng1 As Object = Nothing '開始セル
            Dim rng2 As Object = Nothing '終了セル
            rng1 = objWorkSheet.Cells(StartRow, StartColumn)

            'dtのレコードが０件の場合、エクセルに貼り付ける範囲を調整
            If dt.Rows.Count = 0 Then
                rng2 = objWorkSheet.Cells(StartRow, StartColumn)
            Else
                rng2 = objWorkSheet.Cells(StartRow + (r - 1), StartColumn + (c - 1))
            End If


            Dim rng As Object = Nothing
            rng = objWorkSheet.Range(rng1, rng2)
            rng.Value = tmpArr

            '罫線
            rng.Borders.LineStyle = 1
            rng.Borders.Weight = 1


            '改ページ
            'For r = StartRow To dt.Rows.Count - 1
            '    If Split(objWorkSheet.cells(r, 1).value, "-")(1) Mod 40 = 0 Then objWorkSheet.rows(r).pagebreak = -4135
            'Next


            ReleaseComObject(rng)
            ReleaseComObject(rng2)
            ReleaseComObject(rng1)

            Return True
        Catch ex As Exception
            MsgBox("ExportData" & vbCrLf & "出力に失敗ました。" & vbCrLf & _
                   ex.Message, MsgBoxStyle.Exclamation)
            CloseExcelNoSave()

            Return False
        Finally

            GC.Collect()

        End Try
    End Function
#End Region

#Region "ワークブック保存"

    ''' <summary>
    ''' ワークブックを保存する
    ''' </summary>
    ''' <param name="flgClose">保存後閉じる＝True</param>
    ''' <returns>成功＝True,失敗＝False</returns>
    ''' <remarks></remarks>
    Public Overloads Function SaveWorkBook(Optional ByVal flgClose As Boolean = True) As Boolean
        Try
            objExcel.displayalerts = False
            objWorkBook.SaveAs(Filename:=strSavePath, FileFormat:=56) 'XlFileFormat.xlExcel8)
            objExcel.displayalerts = True
            If flgClose Then
                objExcel.visible = False
            Else
                objExcel.visible = True
            End If

            Return True
        Catch ex As Exception
            MsgBox("保存に失敗ました。" & vbCrLf & _
                   ex.Message, MsgBoxStyle.Exclamation)
            Return False
        Finally
            If flgClose Then CloseExcel()

        End Try

    End Function
#End Region

#Region "ワークブック保存 保存ファイル名指定"

    ''' <summary>
    ''' ワークブックを保存する
    ''' </summary>
    ''' <param name="strSaveFileName">保存ファイル名</param>
    ''' <param name="flgClose">保存後閉じる＝True</param>
    ''' <returns>成功＝True,失敗＝False</returns>
    ''' <remarks></remarks>
    Public Overloads Function SaveWorkBook(ByVal strSaveFileName As String, Optional ByVal flgClose As Boolean = True) As Boolean
        Try
            objExcel.displayalerts = False
            objWorkBook.SaveAs(Filename:=strSaveFileName, FileFormat:=56) 'XlFileFormat.xlExcel8)
            objExcel.displayalerts = True
            If flgClose Then
                objExcel.visible = False
            Else
                objExcel.visible = True
            End If

            Return True
        Catch ex As Exception
            MsgBox("保存に失敗ました。" & vbCrLf & _
                   ex.Message, MsgBoxStyle.Exclamation)
            Return False
        Finally
            If flgClose Then CloseExcel()

        End Try

    End Function
#End Region

#Region "エクセル解放"

    ''' <summary>
    ''' Excelの破棄
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CloseExcel()

        If Not IsNothing(objWorkSheet) Then ReleaseComObject(objWorkSheet)
        If Not IsNothing(objWorkSheets) Then ReleaseComObject(objWorkSheets)
        If Not IsNothing(objWorkBook) Then
            ' objWorkBook.close()
            ReleaseComObject(objWorkBook)
            objWorkBook = Nothing
        End If

        If Not IsNothing(objWorkBooks) Then

            ReleaseComObject(objWorkBooks)
            objWorkBooks = Nothing
        End If
        If Not IsNothing(objExcel) Then
            'objExcel.Quit()
            ReleaseComObject(objExcel)
            objExcel = Nothing
        End If

        GC.Collect()


    End Sub
#End Region

#Region "エクセルを保存せず破棄"

    Public Sub CloseExcelNoSave()
        If objExcel Is Nothing Then Exit Sub

        objExcel.displayalerts = False
        objWorkBook.close()
        objExcel.displayalerts = True
        Marshal.ReleaseComObject(objExcel)

        CloseExcel()

    End Sub
#End Region

#Region "エクセルを保存せず破棄"

    Public Sub CloseExcelNoSave(ByVal ex As Object)
        If ex Is Nothing Then Exit Sub

        Marshal.ReleaseComObject(ex)

        CloseExcel()

    End Sub
#End Region

    Private disposedValue As Boolean = False        ' 重複する呼び出しを検出するには

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: 他の状態を解放します (マネージ オブジェクト)。
            End If

            ' TODO: ユーザー独自の状態を解放します (アンマネージ オブジェクト)。
            GC.Collect()

            ' TODO: 大きなフィールドを null に設定します。
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' このコードは、破棄可能なパターンを正しく実装できるように Visual Basic によって追加されました。
    Public Sub Dispose() Implements IDisposable.Dispose
        ' このコードを変更しないでください。クリーンアップ コードを上の Dispose(ByVal disposing As Boolean) に記述します。
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
